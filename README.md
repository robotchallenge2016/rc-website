# Robot Challenge Landing Page

### System dependencies
* NodeJS & NPM
* Webpack

### Installing all necessary project dependencies
```
npm install
```

### Running development application
```
npm start
```

### Building production application
```
npm run build
```

### Updating application deployed the in Docker container
```
npm run docker-update
```
