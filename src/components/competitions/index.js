require('./competitions.scss');

import router from './router';

export default angular
	.module('rcLandingPageApp.components.competitions', [], router);
