let template = require('./competitions.html');

const COMPETITIONS_ROUTER = {
	competitionsState: {
		url: '/competitions',
		template,
	}
};

export default $stateProvider => $stateProvider
	.state('root.competitions', COMPETITIONS_ROUTER.competitionsState);
