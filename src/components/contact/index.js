require('./contact.scss');

import router from './router';

export default angular.module('rcLandingPageApp.components.contact', [], router);
