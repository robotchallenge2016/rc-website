let template = require('./contact.html');

const CONTACT_ROUTER = {
	contactState: {
		url: '/contact',
		template,
	}
};

export default $stateProvider => $stateProvider
	.state('root.contact', CONTACT_ROUTER.contactState);
