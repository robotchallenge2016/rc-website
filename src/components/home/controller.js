export default class HomeController {
	static get $inject() {
		return ['homeNews'];
	}
	constructor(homeNews) {
		this.homeNews = homeNews.data;
	}
}
