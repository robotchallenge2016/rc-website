require('./home.scss');

import router from './router';
import controller from './controller';

export default angular
	.module('rcLandingPageApp.components.home', [], router)
	.controller('HomeController', controller);
