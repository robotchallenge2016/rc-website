let template = require('./home.html');
let homeNews = require('./../news/resolver').homeNews;

const HOME_ROUTER = {
	homeState: {
		url: '/home',
		template,
		controller: 'HomeController',
		controllerAs: '$ctrl',
		resolve: { homeNews }
	}
};

export default $stateProvider => $stateProvider
	.state('root.home', HOME_ROUTER.homeState);
