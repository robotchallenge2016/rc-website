import competitions from './competitions/';
import contact from './contact/';
import home from './home/';
import news from './news/';
import participants from './participants/';
import rules from './rules';
import results from './results/';
import sponsors from './sponsors/';

const RC_LANDING_PAGE_COMPONENTS = angular.module('rcLandingPageApp.components', [
	competitions.name,
	contact.name,
	home.name,
	news.name,
	participants.name,
	rules.name,
	results.name,
	sponsors.name,
]);

export default RC_LANDING_PAGE_COMPONENTS;
