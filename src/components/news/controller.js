export default class NewsController {
	static get $inject() {
		return ['news'];
	}
	constructor(news) {
		this.news = news.data;
	}
}
