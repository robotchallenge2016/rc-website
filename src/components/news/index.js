require('./news.scss');

import router from './router';
import controller from './controller';
import service from './service';

export default angular
	.module('rcLandingPageApp.components.news', [], router)
	.controller('NewsController', controller)
	.service('newsService', service);
