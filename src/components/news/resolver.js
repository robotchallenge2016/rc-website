const HOME_NEWS_LIMIT = 3;

let news = ['newsService', 'context', (newsService, context) => newsService.getNews(context.selectedEdition)];
let homeNews = ['newsService', 'context',
	(newsService, context) => newsService.getNews(context.selectedEdition, HOME_NEWS_LIMIT)];

module.exports = { news, homeNews };
