let template = require('./news.html');
let news = require('./resolver').news;

const NEWS_ROUTER = {
	newsState: {
		url: '/news',
		template,
		controller: 'NewsController',
		controllerAs: '$ctrl',
		resolve: { news }
	}
};

export default $stateProvider => $stateProvider
	.state('root.news', NEWS_ROUTER.newsState);
