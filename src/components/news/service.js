export default class NewsService {
	static get $inject() {
		return ['$http'];
	}
	constructor($http) {
		this.$http = $http;
	}
	getNews(edition, limit = null, offset = null) {
		let params = { limit, offset };
		const url = '/rc-webapp/v1/website/tournament/edition/guid/' + edition.guid + '/notice/all';
		return this.$http.get(url, { params });
	}
}
