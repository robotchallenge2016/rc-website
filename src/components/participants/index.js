require('./participants.scss');

import controller from './controller';
import router from './router';
import service from './service';

export default angular
	.module('rcLandingPageApp.components.participants', [], router)
	.controller('ParticipantsController', controller)
	.service('participantsService', service);
