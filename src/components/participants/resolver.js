let robots = ['participantsService', 'context', (participantsService, context) => {
	console.log('participantsService', participantsService);
	return participantsService.getRobots(context.selectedEdition);
}];

module.exports = { robots };
