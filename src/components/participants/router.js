let template = require('./participants.html');
let robots = require('./resolver').robots;

const PARTICIPANTS_ROUTER = {
	participantsState: {
		url: '/participants',
		controller: 'ParticipantsController',
		controllerAs: '$ctrl',
		template,
		resolve: { robots }
	}
};

export default $stateProvider => $stateProvider
	.state('root.participants', PARTICIPANTS_ROUTER.participantsState);
