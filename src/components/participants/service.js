export default class ParticipantsService {
	static get $inject() {
		return ['$http'];
	}
	constructor($http) {
		this.$http = $http;
	}
	getRobots(edition, limit = null, offset = null) {
		let params = { limit, offset };
		const url = `/rc-webapp/v1/tournament/edition/guid/${edition.guid}/robot/all/full`;
		return this.$http.get(url, { params });
	}
}
