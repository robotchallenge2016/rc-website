import _ from 'lodash';

const POLLING_THRESHOLD = 5000;

export default class ResultsController {
	static get $inject() {
		return ['$scope', '$state', '$timeout', '$window', '$location', 'context', 'resultsService'];
	}
	constructor($scope, $state, $timeout, $window, $location, context, resultsService) {
		this.$state = $state;
		this.$timeout = $timeout;
		this.$window = $window;
		this.$location = $location;
		this.resultsService = resultsService;

		this._setInitialEdition(context.editions);
		$scope.$on('$stateChangeStart', () => this.$timeout.cancel(this.polling));
	}

	_setInitialEdition(editions) {
		this.editions = editions;
		if (this.$state.params.editionGuid) {
			let guid = this.$state.params.editionGuid;
			let foundEdition = _.find(this.editions, { guid });
			this._selectCurrentEdition(foundEdition);
		} else {
			this.handleSelectEdition(this.editions[0])
		}
	}
	handleSelectEdition(edition) {
		this._setEditionState(edition.guid);
		this._clearCurrentContext();
		this._selectCurrentEdition(edition);
	}
	_setEditionState(editionGuid) {
		this.$state.go(this.$state.current.name, { editionGuid }, { reload: false, notify: false });
	}
	_selectCurrentEdition(edition) {
		this.selectedEdition = edition;

		this.resultsService
			.getCompetitions(edition)
			.then(response => this._setCompetitions(response.data));
	}
	_clearCurrentContext() {
		this.results = [];
		this.competitions = [];
		this.selectedCompetition = null;
		this.selectedCompetitionType = null;

		this.$timeout.cancel(this.polling);
	}

	_setCompetitions(competitions) {
		this.competitions = competitions;
		if(competitions.length > 0) {
			let selectedCompetition = this.competitions[0];
			if (this.$state.params.competitionGuid) {
				let guid = this.$state.params.competitionGuid;
				let foundCompetition = _.find(this.competitions, { guid });
				if(foundCompetition) {
					selectedCompetition = foundCompetition;
				}
			}
			this.handleSelectCompetition(selectedCompetition);
		}
	}
	handleSelectCompetition(competition) {
		this._setCompetitionState(competition.guid);
		this._selectCurrentCompetition(competition);
	}
	_setCompetitionState(competitionGuid) {
		const editionGuid = this.selectedEdition.guid;
		this.$state.go(this.$state.current.name, { competitionGuid, editionGuid }, { reload: false, notify: false });
	}
	_selectCurrentCompetition(competition) {
		this.selectedCompetition = competition;
		this.selectedCompetitionType = competition.competitionSubtype.competitionType.name.toLowerCase();
		this._getCompetitionResults(this.selectedCompetition);
		this.$timeout.cancel(this.polling);
		this._pollResults();
	}
	_getCompetitionResults(competition) {
		this.resultsService
			.getResults(this.selectedEdition, this.selectedCompetitionType, competition.guid)
			.then(results => this.results = results);
	}
	_pollResults() {
		this.polling = this.$timeout(() => {
			this._getCompetitionResults(this.selectedCompetition);
			this._pollResults();
		}, POLLING_THRESHOLD);
	}

	goFullscreen() {
		let url = `${this.$location.absUrl()}&fullscreen=true`;
		let attrs = [
			'status=no',
			'fullscreen=yes',
			'toolbar=no',
			'height=600',
			'width=800'
		].join(',');
		this.$window.open(url, null, attrs);
	}
}
