require('./results.scss');

import controller from './controller';
import router from './router';
import service from './service';

export default angular
	.module('rcLandingPageApp.components.results', [], router)
	.controller('ResultsController', controller)
	.service('resultsService', service);
