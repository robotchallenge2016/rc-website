let competitions = ['resultsService', 'context', (resultsService, context) =>
	resultsService.getCompetitions(context.selectedEdition)];

let tournament = ['resultsService', resultsService => resultsService.retrieveContext()];
let tournamentEdition = ['resultsService', resultsService => resultsService.retrieveEditionsContext()];
let context = ['$rootScope', 'tournament', 'tournamentEdition', ($rootScope, tournament, tournamentEdition) => {
	let context = {
		tournament: tournament.data,
		editions: tournamentEdition.data
	};
	if(context.editions.length > 0) {
		context.selectedEdition = context.editions[0];
	}
	return $rootScope.context = context;
}];

module.exports = { competitions, context, tournament, tournamentEdition };
