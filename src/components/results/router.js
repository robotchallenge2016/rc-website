let template = require('./results.html');
let competitions = require('./resolver').competitions;

const RESULTS_ROUTER = {
	resultsState: {
		url: '/results?competitionGuid?editionGuid?fullscreen',
		controller: 'ResultsController',
		controllerAs: '$ctrl',
		template,
		resolve: { competitions }
	}
};

export default $stateProvider => $stateProvider
	.state('root.results', RESULTS_ROUTER.resultsState);
