export default class ResultsService {
	static get $inject() {
		return ['$http'];
	}
	constructor($http) {
		this.$http = $http;
	}
	retrieveContext() {
		return this.$http.get("/rc-webapp/v1/context/tournament");
	};

	retrieveEditionsContext() {
		return this.$http.get("/rc-webapp/v1/context/tournament/edition");
	};
	getCompetitions(edition) {
		const url = `/rc-webapp/v1/tournament/edition/guid/${edition.guid}/competition/all`;
		return this.$http.get(url);
	}

	getResults(edition, competition, guid) {
		const url = `/rc-webapp/v1/tournament/edition/guid/${edition.guid}/ranking/${competition}/guid/${guid}`;
		return this.$http
			.get(url)
			.then(response => this._normalizeResults(competition, response.data));
	}
	_normalizeResults(competition, results) {
		competition = _.capitalize(competition);
		angular.forEach(results, item =>
			item = (this[`_normalize${competition}Result`] || angular.identity)(item)
		);
		return results
	}
	_normalizeSumoResult(item) {
		item.normalizedResult = `${item.points} pkt`;
		return item;
	}
	_normalizeLinefollowerResult(item) {
		item.normalizedResult = `${item.result} ms`;
		return item;
	}
	_normalizeFreestyleResult(item) {
		item.normalizedResult = `
			pierwszy: ${item.firstPlaceCount},
			drugi: ${item.secondPlaceCount},
			trzeci: ${item.thirdPlaceCount}`;
		return item;
	}
}
