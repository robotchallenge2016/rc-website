require('./rules.scss');

import router from './router';

export default angular
	.module('rcLandingPageApp.components.rules', [], router);
