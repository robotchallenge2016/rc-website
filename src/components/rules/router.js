let template = require('./rules.html');

const RULES_ROUTER = {
	rulesState: {
		url: '/rules',
		template,
	}
};

export default $stateProvider => $stateProvider
	.state('root.rules', RULES_ROUTER.rulesState);
