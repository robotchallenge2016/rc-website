require('./sponsors.css');

import router from './router';

export default angular.module('rcLandingPageApp.components.sponsors', [], router);
