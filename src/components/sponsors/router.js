let template = require('./sponsors.html');

const SPONSORS_ROUTER = {
	sponsorsState: {
		url: '/sponsors',
		template,
	}
};

export default $stateProvider => $stateProvider
	.state('root.sponsors', SPONSORS_ROUTER.sponsorsState);
