let tournament = require('./components/results/resolver').tournament;
let tournamentEdition = require('./components/results/resolver').tournamentEdition;
let context = require('./components/results/resolver').context;

const ROOT_ROUTER = {
	rootState: {
		abstract: true,
		url: '',
		template: '<ui-view/>',
		resolve: { context, tournament, tournamentEdition }
	}
};

export default ($compileProvider, $urlRouterProvider, cfpLoadingBarProvider, $stateProvider) => {
	// $compile configuration
    $compileProvider.debugInfoEnabled(false);

    // ui-router configuration
    $urlRouterProvider.otherwise('home');

	// loading bar configuration
	cfpLoadingBarProvider.includeSpinner = false;


	$stateProvider.state('root', ROOT_ROUTER.rootState)
}
