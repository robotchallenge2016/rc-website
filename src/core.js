import 'angular-sanitize';
import 'angular-loading-bar';
import 'angular-ui-router';
import 'angular-ui-bootstrap';
import 'bootstrap-css-only';
import 'animate.css';
import 'moment';
import 'lodash';
import 'moment/locale/pl';

const RC_LANDING_PAGE_CORE = angular.module('rcLandingPageApp.core', [
	'ngSanitize',
	'ui.router',
	'ui.bootstrap',
	'angular-loading-bar'
]);

export default RC_LANDING_PAGE_CORE;
