import angular from 'angular';

import core from './core';
import config from './config';
import run from './run';
import components from './components';

import layout from './common/scss/layout.scss';

const RC_LANDING_PAGE_APP = angular
	.module('rcLandingPageApp', [
		core.name,
		components.name
	], config)
	.run(run);

export default RC_LANDING_PAGE_APP;
