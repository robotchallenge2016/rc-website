import moment from 'moment';

export default ($rootScope, $state) => {
	$rootScope.moment = moment;
	$rootScope.$state = $state;
};
