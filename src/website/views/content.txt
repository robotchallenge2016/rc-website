O Zawodach
--------------------

Robot Challenge 2016  to zawody skupiające się na robotyce i sztucznej inteligencji, skierowane do uczniów szkół średnich oraz studentów. Zawody dają szansę uczestnikom:
- zmierzenia się z innymi utalentowanymi uczniami i studentami,
- prezentacji własnoręcznie skonstruowanych robotów,
rywalizacji o atrakcyjne nagrody.

Konkurencje
--------------------

Linefollower
Robot ma za zadanie w jak najkrótszym czasie pokonać wytyczoną na podłożu trasę.

Sumo
Dwa roboty umieszczone na macie po sygnale startowym mają za zadanie sprawić, by przeciwnik jako pierwszy wypadł z maty. Roboty występują w różnych kategoriach wagowych i rozmiarowych.

Freestyle
Roboty będące autorskimi konstrukcjami. Ich zadaniem jest zaskoczenie i zadziwienie zarówno widzów jak i jurorów.

Kontakt
--------------------

Zespół Szkół Łączności
ul. Monte Cassino 31
30-337 Kraków
Email:
rc@tl.krakow.pl

Home
--------------------

Zapraszamy na pierwsze zawody robotów w Zespole Szkół Łączności !
Zbuduj własnego robota i weż udział w zawodach robotów organizowanych przez ZSŁ.

Ogłoszenie 1:
O Zawodach
Robot Challenge 2016 to zawody skupiające się na robotyce i sztucznej inteligencji, skierowane do uczniów szkół średnich oraz studentów. Zawody dają szansę uczestnikom:
- zmierzenia się z innymi utalentowanymi uczniami i studentami,
- prezentacji własnoręcznie skonstruowanych robotów,
- rywalizacji o atrakcyjne nagrody.

Ogłoszenie 2:
Sponsorzy:
W organizację zawodów wkładamy własne siły i pomysły, lecz ze względu na spodziewaną dużą liczbę uczestników
( zarówno uczniów szkół średnich jak i studentów) gorąco liczymy na Państwa wsparcie, wierząc, że dla Państwa firmy będzie to wspaniała promocja i reklama.
Pomoc przeznaczymy na:
- warsztaty konstrukcji roobtów dla uczniów szkoły,
- druk plakatów i dyplomów
- zakup upominków,
- NAGRODY.

Ogłoszenie 3:
Warsztaty dla uczniów
W związku z organizacją zawodów robotów w ZSŁ, chcielibyśmy zachęcić uczniów szkoły do wzięcia udziału w warsztatch budowy robotów. Warsztaty będą obejmowały:
- podstawy elektroniki (dla osób z klas o innym profilu niż elektroniczny),
- projektowanie prostych układów elektronicznych,
- projektowanie płytek drukowanych,
- programowanie mikrokontrolerów.

Sponsorzy
--------------------
W organizację zawodów wkładamy własne siły i pomysły, lecz ze względu na spodziewaną dużą liczbę uczestników
( zarówno uczniów szkół średnich jak i studentów) gorąco liczymy na Państwa wsparcie, wierząc, że dla Państwa firmy będzie to wspaniała promocja i reklama.
Pomoc przeznaczymy na:
- warsztaty konstrukcji roobtów dla uczniów szkoły,
- druk plakatów i dyplomów,
- zakup upominków,
- NAGRODY.

Forma pomocy:
- pieniężna,
- materialna (nagrody rzeczowe lub gadżety firmowe),
- inna według uznania i możliwości,
- każda pomoc jest dla nas cenna.

W zamian oferujemy:
- reklama podczas zawodów,
- wpis na stronę internetową szkoły oraz stronę zawodów pod tytułem "sponsor",
- polecanie usług firmy,
- możliwość wykorzystania własnych materiałów promocyjnych z tytułem "sponsor" w trakcie trwania zawodów ( kolportaż wśród uczestników lub punkt promocyjny),

Jesteśmy również otwarci na inne sposoby promocji.