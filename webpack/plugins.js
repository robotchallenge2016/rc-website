var CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = [
	new CopyWebpackPlugin([{
		from: 'src/index.html',
		to: 'index.html'
	}, {
		from: 'src/common',
		to: 'common'
	}])
];
