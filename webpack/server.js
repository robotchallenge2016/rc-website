var path = require('path');

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0"; // Avoids DEPTH_ZERO_SELF_SIGNED_CERT error for self-signed certs

var defaultProxy = {
	target: {
		host: '192.168.99.101',
		protocol: 'https:'
	},
	changeOrigin: true,
	secure: false
};

module.exports = {
	outputPath: 'http://localhost:8079',
	port: 8079,
	open: true,
	historyApiFallback: true,
	proxy: {
		'/rc-webapp/*': defaultProxy
	}
}
